package tk.hack5.cipherchallenge

class Text private constructor(val text: CharSequence, val safeChars: CharSequence) : CharSequence by safeChars {

    override fun toString(): String {
        return text.toString()
    }

    fun replaceSafe(safe: CharSequence): Text {
        if (safe.length != safeChars.length) {
            // changed length of safe chars, must discard all spacing data
            return Text(safe)
        }
        var i = 0
        return Text(buildString(length) {
            text.indices.forEach {
                append(if (text[it].isSafe()) safe[i++] else text[it])
            }
        })
    }

    companion object {
        operator fun invoke(text: CharSequence): Text {
            val safeChars = text.filter { it.isSafe() }.map { it.toLowerCase() }.joinToString("")
            return Text(text, safeChars)
        }
    }
}

fun Char.isSafe() = isLetterOrDigit()