package tk.hack5.cipherchallenge

import kotlin.math.min

val WORDS_3K: List<String> get() {
    return object{}::class.java.getResource("/The-Oxford-3000/The_Oxford_3000.txt").readText().split("\n").map {
        it.trim().toLowerCase().filter { char -> char in 'a'..'z' }
    }
}
val WORDS_3K_FAST = WORDS_3K.filter { it.length in 4..10 }
val WORDS_3K_FAST_MAX_LEN = WORDS_3K_FAST.map { it.length }.maxOrNull()!!
const val MATCH_THRESHOLD = 3f / 100f

fun rateEnglish(text: CharSequence): Int {
    require(text.length >= 100)
    var matches = 0
    var offset = 0
    offset@while (offset < text.length) {
        len@for (len in (WORDS_3K_FAST_MAX_LEN downTo 4)) {
            if (len + offset > text.length)
                continue@len
            if (text.slice(offset until offset + len) in WORDS_3K_FAST) {
                matches++
                offset += len
                continue@offset
            }
        }
        offset++
    }
    return matches
}

fun isEnglish(text: CharSequence): Boolean {
    return isEnglish(rateEnglish(text), text.length)
}

fun isEnglish(rating: Int, length: Int): Boolean {
    return rating > MATCH_THRESHOLD * length
}