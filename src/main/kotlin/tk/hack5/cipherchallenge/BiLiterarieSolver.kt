package tk.hack5.cipherchallenge

import java.nio.Buffer
import java.nio.ByteBuffer
import java.nio.IntBuffer
import java.util.*

fun solveBiLiterarie(text: Text): Text? {
    val distinct = text.toList().distinct()
    if (distinct.size != 2) return null // TODO fail fast when != 2, without finding all distinct
    solveBiLiterarie(text, distinct[0])?.let {
        return it
    }
    solveBiLiterarie(text, distinct[1])?.let {
        return it
    }
    return null
}

fun solveBiLiterarie(text: Text, zero: Char): Text? {
    val bits = BitSet(5)
    return text.replaceSafe(String(text.chunked(5).map { chunk ->
        chunk.forEachIndexed { i, it ->
            bits.set(4 - i, it != zero)
        }
        val char = (bits.toByteArray().singleOrNull() ?: 0)
        if (char !in 0 until 26) {
            return null
        }
        (char + 'a'.toInt()).toChar()
    }.toCharArray()))
}