package tk.hack5.cipherchallenge

import java.lang.IndexOutOfBoundsException
import kotlin.math.abs

fun calculateIC(string: Text, normalize: Int = 26): Float {
    var ret = 0F
    if (string.length <= 1) return 0F
    val stringLength = string.length.toFloat()
    for (char in ('a'..'z')) {
        val count = string.count { it == char }.toFloat()
        ret += (count / stringLength) * ((count - 1) / (stringLength - 1))
    }
    return ret * normalize
}

val ENGLISH = 1.73
val MAX_DIFF = 0.15
val STRICT_MAX_DIFF = 0.15
val MAX_ALPHABETS = 10

fun countAlphabets(string: Text): Int? {
    val indices = (1..MAX_ALPHABETS).map { alphabets ->
        string.step(alphabets).map {
            calculateIC(it)
        }.average()
    }
    return indices.indexOfFirst { it > ENGLISH - MAX_DIFF && it < ENGLISH + MAX_DIFF }.also {
        if (it < 0) return null
    } + 1
}

fun Text.step(step: Int): List<Text> =
    (0 until step).map {
        Text(filterIndexed { index, _ -> index % step == it })
    }

fun unstep(text: List<CharSequence>): Text {
    var i = 0
    var j = 0
    val l = text.sumBy(CharSequence::length)
    return Text(buildString(l) {
        while (true) {
            try {
                append(text[j][i])
            } catch (ignored: IndexOutOfBoundsException) {}
            if (text.size == ++j) {
                j = 0
                if (text[j].length == ++i)
                    break
            }
        }
    })
}