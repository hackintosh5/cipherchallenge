package tk.hack5.cipherchallenge

fun main() {
    val text = getText()
    println(text.length)
    println(rateEnglish(text))
    println(solve(text))
}

fun getText(): Text = Text(object{}::class.java.getResource("/input.txt").readText())