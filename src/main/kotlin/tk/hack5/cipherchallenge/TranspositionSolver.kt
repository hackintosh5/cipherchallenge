package tk.hack5.cipherchallenge

fun solveTransposition(text: Text): Text? {
    var topRating: Triple<Int, Text?, List<Int>?> = Triple(0, null, null)
    val minWords = text.length * MATCH_THRESHOLD
    for (len in 3..10) {
        if (text.length % len != 0) continue
        val cols = text.step(len)
        for (key in combinations(len)) {
            if (key == key.indices.toList()) continue
            val unstepped = unstep(cols.slice(key))
            rateEnglish(unstepped).let {
                if (it > topRating.first) {
                    topRating = Triple(it, unstepped, key)
                }
            }
        }
        if (topRating.first > minWords) {
            println(topRating.third)
            return text.replaceSafe(topRating.second!!)
        }
    }
    return null
}