package tk.hack5.cipherchallenge

val ALPHABET = 'a'..'z'
val FREQENCIES = "etaoinsrhdlucmfywgpbvkxqjz"
val DIGITS = '0'..'9'

fun solveMonoAlphabetical(text: Text): Text? {
    val freq = buildFrequencies(text)
    val sorted = freq.toList().sortedByDescending { it.second }
    val off = 'e'.toInt() - sorted.first().first.toInt()
    return text.replaceSafe(if (((sorted[1].first - 'a' + off) posRem 26).toChar() + 'a'.toInt() == 't') {
        if (off == 0) {
            // Already have English frequencies, but we wouldn't have got here if it wasn't English. Try obscure stuff
            val reversed = text.reversed()
            if (rateEnglish(reversed) > rateEnglish(text) + text.length / 1000) {
                return text.replaceSafe(reversed)
            }
            return null
        }
        solveCaesar(text, off)
    } else {
        solveMapping(text, sorted)
    })
}

fun solveCaesar(text: CharSequence, off: Int): String {
    return text.map { ((it - 'a' + off) posRem 26).toChar() + 'a'.toInt() }.toCharArray().joinToString("")
}

private fun matches(text: CharSequence, offset: Int, target: String, mapping: Map<Char, Char>): Map<Char, Char>? {
    val ret = mutableMapOf<Char, Char>()
    target.forEachIndexed { i, char ->
        val actual = text[i + offset]
        when (char) {
            '*' -> {
                /* no-op */
            }
            '?' -> {
                if (actual in mapping.values)
                    return null
            }
            in mapping.keys -> {
                if (actual != mapping[char])
                    return null
            }
            in DIGITS -> {
                when (actual) {
                    ret[char] -> {
                        /* no-op */
                    }
                    in ret.values -> return null
                    in mapping.values -> return null
                    else -> ret[char] = actual
                }
            }
            else -> return null
        }
    }
    return ret
}

private fun findChar(text: CharSequence, target: String, mapping: MutableMap<Char, Char>): Map<Char, Char>? {
    val possible = mutableMapOf<Map<Char, Char>, Int>()
    text.fold("") { acc, char ->
        if (acc.length >= target.length) {
            matches(acc + char, acc.length - target.length + 1, target, mapping)?.let {
                possible[it] = (possible[it] ?: 0) + 1
            }
        }
        acc + char
    }
    return possible.maxByOrNull { it.value }?.key
}

fun deriveAbstractMappings(text: CharSequence, mapping: MutableMap<Char, Char>) {
    var mapped: Int
    for (matchLimit in listOf(.90f, .85f, .80f, .75f, .70f)) {
        for (minLength in 10 downTo 4) {
            do {
                mapped = mapping.size
                var i = 0
                offset@ while (i < text.length) {
                    abstract@ for (length in 16 downTo minLength) {
                        if (i + length > text.length) {
                            continue@abstract
                        }
                        val abstract = abstractifyWord(text.substring(i, i + length))
                        val words = WORDS_ABSTRACT_MAP[abstract] ?: continue
                        var matched: Int
                        var topMatched = 0
                        var topMatchedWord: String? = null
                        word@ for (word in words) {
                            matched = 0
                            for ((wordOffset, it) in word.withIndex()) {
                                val ciphered = mapping[it]
                                        ?: if (mapping.containsValue(text[i + wordOffset])) {
                                            continue@word
                                        } else {
                                            continue
                                        }
                                if (ciphered != text[i + wordOffset]) {
                                    continue@word
                                }
                                matched++
                            }
                            if (matched > topMatched) {
                                topMatched = matched
                                topMatchedWord = word
                            } else if (matched == topMatched) {
                                topMatchedWord = null
                            }
                        }
                        if (topMatchedWord == null) {
                            continue@abstract
                        }

                        if (topMatched < length * matchLimit || topMatched == length) {
                            break@abstract
                        }
                        println(topMatchedWord)
                        println(topMatched)
                        for ((wordOffset, char) in topMatchedWord.withIndex()) {
                            mapping[char] = text[i + wordOffset]
                        }
                        println(mapping)
                        i += length
                        continue@offset
                    }
                    i++
                }
            } while (mapping.size != mapped)
        }
    }
}

fun solveMapping(text: CharSequence, sorted: List<Pair<Char, Int>>): String {
    println(sorted)
    val mapping = mutableMapOf<Char, Char>() // key=plain, value=cipher
    fun find(target: String, vararg char: Char): Unit? {
        val ret = findChar(text, target, mapping) ?: return null
        if (ret.size > char.size) {
            error("${ret.size} > ${char.size}")
        }
        ret.forEach {
            val key = char[it.key - '0']
            when (mapping[key]) {
                it.value -> return@forEach
                null -> { /* no-op */ }
                else -> return null
            }
            if (it.value in mapping.values) {
                return null
            }
            mapping[char[it.key - '0']] = it.value
        }
        return Unit
    }
    mapping['e'] = sorted[0].first
    mapping['t'] = sorted[1].first
    println(mapping)
    find("t0e", 'h')!!
    find("th0t", 'a')!!
    find("ha0e", 'v')!!
    find("he0e", 'r')!!
    val partA = find("01ter2e3t", 'i', 'n', 'c', 'p')
    find("at0", 'i') // frequent combo, not sure why :P
    find("0ith", 'w')
    find("whi0h", 'c')
    partA ?: run {
        find("0c12t", 's', 'o', 'u')!!
    }

    println(mapping)
    deriveAbstractMappings(text, mapping)
    find("ne0t", 'x')
    find("na0i", 'z')

    val sortedMissing = sorted.filter { it.second != 0 }.map { it.first } - mapping.values
    val textMissing = FREQENCIES.toList() - mapping.keys
    println(textMissing)
    return permute(textMissing).map { textMissingChosen ->
        val inverseMapping = mapping.map { it.value to it.key }.toMap() + sortedMissing.zip(textMissingChosen)
        val ret = text.map {
            inverseMapping[it] ?: it.toUpperCase()
        }.joinToString("")
        println(ret)
        println(rateEnglish(ret))
        ret
    }.maxByOrNull {
        rateEnglish(it)
    }!!

}

fun buildFrequencies(text: Text): Map<Char, Int> {
    val ret = mutableMapOf<Char, Int>()
    for (char in text) {
        ret[char] = (ret[char] ?: 0) + 1
    }
    return ret
}

infix fun Int.posRem(other: Int): Int {
    return rem(other).let {
        if (it < 0) it + other else it
    }
}