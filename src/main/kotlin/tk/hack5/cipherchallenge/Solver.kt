package tk.hack5.cipherchallenge

fun solve(text: Text): Text {
    var ret = text
    while (true) {
        ret = solveSingle(ret) ?: break
    }
    return ret
}

fun solveSingle(text: Text): Text? {
    solveBiLiterarie(text)?.let {
        return it
    }
    val alphabets = countAlphabets(text) ?: error("Unknown alphabets")
    println("alphabets =\t$alphabets")
    val substituted = if (alphabets > 1) {
        solvePolyAlphabetical(text, alphabets)
    } else {
        solveMonoAlphabetical(text)
    }
    return substituted ?: if (isEnglish(text)) null else solveTransposition(text)
}