package tk.hack5.cipherchallenge

import kotlin.system.measureNanoTime

fun solvePolyAlphabetical(input: Text, alphabets: Int): Text {
    /*
     * First try a vignere (caesar on each column)
     * If that fails, enumerate all keys and try every single one.
     * There is no way I know of to optimise this case.
     * It remains tractable.
     */
    var topRated = 0
    var topRatedText: Text? = null
    val stepped = input.step(alphabets)
    val frequencies = stepped.map { buildFrequencies(it).toList().sortedByDescending { pair -> pair.second } }
    println(alphabets)
    combinations(2, alphabets).forEach {
        val new = unstep(stepped.mapIndexed { i, col -> solveCaesar(col, 'e' - frequencies[i][it[i]].first) })
        if (countAlphabets(new) == 1) {
            return new
        }
    }
    val newAlphabets = countAlphabets(topRatedText!!) ?: error("Unknown alphabets")
    return input.replaceSafe(
            if (newAlphabets < alphabets) {
                topRatedText
            } else {
                solveAdvancedPolyAlphabetical(input, alphabets)
            }
    )
}

fun solveAdvancedPolyAlphabetical(input: CharSequence, alphabets: Int): CharSequence {
    val data = PolyAlphabeticalData(alphabets)
    kasiskiExamination(input, alphabets)
    TODO()
}

fun kasiskiExamination(input: CharSequence, alphabets: Int): List<UnresolvedKasiskiResult> {
    for (len in 3..alphabets) {
        val stuff = mutableListOf<UnresolvedKasiskiResult>()
        (0 until input.length - len).forEach {
            stuff += UnresolvedKasiskiResult(it, alphabets, input.substring(it, it + len))
        }
        stuff.groupingBy { it }.eachCount().forEach { (result, count) ->
            if (count > 1) {
                println("repeat $count\t$result")
            }
        }
    }
    TODO()
}

@Suppress("DataClassPrivateConstructor")
data class UnresolvedKasiskiResult(val startOffset: Int, val alphabets: Int, val ciphertext: CharSequence) {

    fun getAlphabetAt(offset: Int) = (startOffset + offset).rem(alphabets)
    fun withAlphabet() = ciphertext.mapIndexed { i, it -> getAlphabetAt(i) to it }

    override fun hashCode(): Int {
        var result = startOffset.rem(alphabets)
        result = 31 * result + alphabets
        result = 31 * result + ciphertext.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnresolvedKasiskiResult

        if (startOffset.rem(alphabets) != other.startOffset.rem(alphabets)) return false
        if (alphabets != other.alphabets) return false
        if (ciphertext != other.ciphertext) return false

        return true
    }
}

inline class PolyAlphabeticalData(val alphabetData: List<MutableMap<Char, Char>> /* key=plain, value=cipher */) {
    constructor(alphabets: Int) : this((0 until alphabets).map { mutableMapOf() })
/*
    fun addAll(data: ResolvedKasiskiResult) {
        data.withAlphabet(alphabetData.size).forEach { (alphabet, char) ->
            alphabetData[alphabet][char] =
        }
    }*/
}