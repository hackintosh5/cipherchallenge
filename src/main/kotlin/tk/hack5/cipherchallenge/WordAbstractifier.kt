package tk.hack5.cipherchallenge

import kotlin.math.min

fun abstractifyWord(text: CharSequence): String {
    val usedChars = mutableMapOf<Char, Char>()
    var nextChar = 'a'
    var ret = ""
    for (char in text) {
        ret += usedChars.getOrPut(char, { nextChar++ })
    }
    return ret
}

val WORDS_ABSTRACT_FULL = WORDS_3K.map { abstractifyWord(it) to it }
val WORDS_ABSTRACT_FAST = WORDS_ABSTRACT_FULL.map { it.first }.filter { it.length in 6..12 }
val WORDS_ABSTRACT_MAP = WORDS_ABSTRACT_FULL.filter { it.first.length in 6..16 }.groupBy { it.first }.mapValues { inner -> inner.value.map { it.second } }.toMap()
const val ABSTRACT_MATCH_THRESHOLD = 11

fun checkAbstractText(string: CharSequence): Boolean {
    require(string.length >= 100)
    val text = string.slice(0 until 100)
    var matches = 0
    var offset = 0
    var matched: Boolean
    while (offset < 96) {
        matched = false
        len@for (len in (min(12, 100 - offset) downTo 6)) {
            if (abstractifyWord(text.slice(offset until offset + len)) in WORDS_ABSTRACT_FAST) {
                matches++
                offset += len
                matched = true
                break@len
            }
        }
        if (!matched) offset++
    }
    println("matches=\t$matches")
    return matches > ABSTRACT_MATCH_THRESHOLD
}