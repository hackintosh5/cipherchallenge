package tk.hack5.cipherchallenge

fun combinations(max: Int): Sequence<List<Int>> {
    return permute((0 until max).toList())
}

fun <T>permute(input: List<T>): Sequence<List<T>> = sequence {
    if (input.size == 1) {
        yield(input)
        return@sequence
    }
    val toInsert = input[0]
    for (perm in permute(input.drop(1))) {
        for (i in 0..perm.size) {
            val newPerm = perm.toMutableList()
            newPerm.add(i, toInsert)
            yield(newPerm)
        }
    }
}

fun <T>permuteListOld(input: List<List<T>>): Sequence<List<T>> = sequence {
    println(input.size)
    input.singleOrNull()?.let {
        yieldAll(it.map(::listOf))
        return@sequence
    }
    input.first().map(::listOf).map { firstAsList ->
        yieldAll(permuteListOld(input.subList(1, input.size)).map {
            firstAsList + it
        })
    }
}
//TODO add version that supports <T>

fun <T>permuteList(input: List<List<T>>): Sequence<List<T>> =
        permuteListInternal(input.map(List<*>::lastIndex)).map { it.mapIndexed { i, choice -> input[i][choice] } }
// each entry of list is the last index
fun permuteListInternal(input: List<Int>): Sequence<IntArray> = sequence {
    val indices = IntArray(input.size) { 0 }
    var offset = input.lastIndex
    yield(indices)
    loop@while (true) {
        if (++indices[offset] > input[offset]) {
            --indices[offset]
            if (--offset < 0)
                return@sequence
        } else {
            if (++offset > input.lastIndex) {
                --offset
            } else {
                for (i in offset..input.lastIndex) {
                    indices[i] = 0
                }
            }
            yield(indices)
        }
    }
}

fun combinations(max: Int, count: Int) = sequence {
    val ret = IntArray(count) { 0 }
    ret[0]--
    var i: Int
    loop@while (true) {
        i = 0
        ret[i]++
        check@while (true) {
            if (ret[i] == max) {
                ret[i++] = 0
                if (i == count) return@sequence
                ret[i]++
                continue@check
            }
            break@check
        }
        yield(ret.toList())
    }
}